/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

public class library {
    @executerpane.MethodAnnotation(signature = "reverse(const char**,int):const char**")
    public String[] reverse(String[] value, int valueLength){
        return reverse_(value, valueLength);
    }
    private native String[] reverse_(String[] value, int valueLength);


    static {
        System.load(new java.io.File(".jni", "library_jni.so").getAbsolutePath());
    }
}
