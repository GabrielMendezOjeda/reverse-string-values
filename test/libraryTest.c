#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "jni_test.h"
#include "library.c"

/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

void testExercise_A() {
    // Given
    const char* name[] = {"Eva"};
    // When
    const char* amount = reverse(name, 1);
    // Then
    assertArrayEquals_String(name, 1, amount, 1);
}

void testExercise_B() {
	// Given
    const char* name[] = {"Eva", "Javier"};
    const char* finalname[] = {"Javier", "Eva"};
    // When
    const char* amount = reverse(name, 2);
    // Then
    assertArrayEquals_String(finalname, 2, amount, 2);
	}

void testExercise_C() {
	// Given
    const char* name[] = {"Eva", "Javier", "Alba"};
    const char* finalname[] = {"Alba", "Javier", "Eva"};
    // When
    const char* amount = reverse(name, 3);
    // Then
    assertArrayEquals_String(finalname, 3, amount, 3);
	}

void testExercise_D() {
	// Given
    const char* name[] = {"Eva", "Javier", "Alba", "Manuel"};
    const char* finalname[] = {"Manuel", "Alba", "Javier", "Eva"};
    // When
    const char* amount = reverse(name, 4);
    // Then
    assertArrayEquals_String(finalname, 4, amount, 4);
	}

void testExercise_E() {
	// Given
    const char* name[] = {"Eva", "Javier", "Alba", "Manuel", "Susana"};
    const char* finalname[] = {"Susana", "Manuel", "Alba", "Javier", "Eva"};
    // When
    const char* amount = reverse(name, 5);
    // Then
    assertArrayEquals_String(finalname, 5, amount, 5);
	}
