#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

const char** reverse(const char** value, int valueLength) {
	const char** result = (const char**)malloc(valueLength * sizeof(const char*));
	int start = 0;
	for (int k = valueLength - 1; k >= 0; k--, start++){
		result [start] = value[k];
		result[k] = value [start];
	}
	return result;
	}
